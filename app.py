from fastapi import FastAPI, UploadFile
from sklearn.linear_model import LogisticRegression, LinearRegression
from src.model_handler import ModelHandler

app = FastAPI(
    title="ModelsLearner",
    version='0.1',
    contact={
        "name": "Dmitriy Razumovskiy",
        "tg": "@cytryx",
        "email": "darazumovskiy@hse.edu.ru",
    }
)

MODELS_LIST = {
    "LogisticRegression": LogisticRegression,
    "LinearRegression": LinearRegression
}
ROOT_LOCAL_PATH = './'


@app.get("/models/types", tags=["model"])
async def return_existing_model_types():
    return {'data': list(MODELS_LIST.keys())}


@app.get("/models/trained", tags=["model"])
async def return_list_of_trained_model():
    return ModelHandler(ROOT_LOCAL_PATH).get_trained_models()


@app.put("/models/create/{model_type}", tags=["model"])
async def create_model_with_params_of_type(model_type: str, params: dict):
    model = ModelHandler(ROOT_LOCAL_PATH).create_model(MODELS_LIST, model_type, params)
    return model


@app.post("/models/train/{uuid}", tags=["model"])
async def train_created_model(uuid: str, x: UploadFile, y: UploadFile):
    ModelHandler(ROOT_LOCAL_PATH).train_model(uuid, x.file, y.file)
    return f"Model {uuid} was trained"


@app.post("/models/predict/{uuid}", tags=["model"])
async def predict_by_trained_model(uuid: str, x: UploadFile):
    model = ModelHandler(ROOT_LOCAL_PATH).predict_model(uuid, x.file)
    return model.to_json(orient='records')


@app.delete("/models/delete/{uuid}", tags=["model"])
async def delete_model(uuid: str):
    ModelHandler(ROOT_LOCAL_PATH).delete_model(uuid)
    return f"Model {uuid} was deleted"
