FROM python:3.9

COPY ./src /code/src
COPY ./tests /code/tests
COPY ./app.py /code
COPY ./requirements.txt /code

WORKDIR /code
RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt
RUN export PYTHONPATH="${PYTHONPATH}:/code"
RUN dvc init --no-scm

EXPOSE 8000
CMD ["/bin/bash", "connect-minio.sh"]
