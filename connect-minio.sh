dvc remote add -d minio s3://somebucketname/
dvc remote modify minio access_key_id admin
dvc remote modify minio secret_access_key admin12345
dvc remote modify minio endpointurl http://minio:9000
dvc remote modify minio version_aware true
mkdir models
mkdir train
dvc add models
dvc add train
uvicorn app:app --host 0.0.0.0 --port 8000