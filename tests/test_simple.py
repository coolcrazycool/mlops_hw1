import src.model_handler
from src.model_handler import ModelHandler
from sklearn.linear_model import LogisticRegression, LinearRegression
import shutil

MODELS_LIST = {
    "LogisticRegression": LogisticRegression,
    "LinearRegression": LinearRegression
}
ROOT_LOCAL_PATH = './'


def test_dvc_commit_and_push_mock(mocker):
    mocker.patch('src.model_handler.dvc_commit_and_push', return_value=None)
    assert src.model_handler.dvc_commit_and_push() is None

def test_create_model(mocker):
    mocker.patch('src.model_handler.dvc_commit_and_push', return_value=None)
    model = ModelHandler(ROOT_LOCAL_PATH).create_model(MODELS_LIST, "LogisticRegression", {})
    assert len(str(model)) == 36
    shutil.rmtree('models', ignore_errors=True)
    shutil.rmtree('predictions', ignore_errors=True)


def test_get_models_list(mocker):
    mocker.patch('src.model_handler.dvc_commit_and_push', return_value=None)
    model = ModelHandler(ROOT_LOCAL_PATH).create_model(MODELS_LIST, "LogisticRegression", {})
    assert ModelHandler(ROOT_LOCAL_PATH).get_trained_models() == [str(model) + ".pickle"]
    shutil.rmtree('models', ignore_errors=True)
    shutil.rmtree('predictions', ignore_errors=True)

def test_base():
    model = ModelHandler(ROOT_LOCAL_PATH)
    assert model.get_trained_models() == []
    shutil.rmtree('models', ignore_errors=True)
    shutil.rmtree('predictions', ignore_errors=True)
