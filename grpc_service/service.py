from grpc_service import grpc_pb2_grpc, grpc_pb2
from src.model import Model
from uuid import uuid4
import pickle as pkl
from pathlib import Path
import os
import pandas as pd
from fastapi import HTTPException
from sklearn.linear_model import LogisticRegression, LinearRegression
from src.model_handler import ModelHandler as mh
import io
import json

MODELS_LIST = {
    "LogisticRegression": LogisticRegression,
    "LinearRegression": LinearRegression
}
ROOT_LOCAL_PATH = './'

class ModelHandler(grpc_pb2_grpc.ModelServicer):
    Path(ROOT_LOCAL_PATH, 'models').mkdir(parents=True, exist_ok=True)
    Path(ROOT_LOCAL_PATH, 'predictions').mkdir(parents=True, exist_ok=True)

    def ModelTypesList(self, request, context):
        __result = grpc_pb2.ModelListResponse(model=list(MODELS_LIST.keys()))
        return __result

    def CreatedModelList(self, request, context):
        __result = grpc_pb2.ModelListResponse(model=mh(ROOT_LOCAL_PATH).get_trained_models())
        return __result

    def ModelCreate(self, request, context):
        __result = grpc_pb2.ModelSimpleResponse(
            message=str(mh(ROOT_LOCAL_PATH).create_model(MODELS_LIST, request.tp, json.loads(request.params)))
        )
        return __result

    def ModelTrain(self, request, context):
        mh(ROOT_LOCAL_PATH).train_model(request.uuid, io.BytesIO(request.x), io.BytesIO(request.y))
        __result = grpc_pb2.ModelSimpleResponse(
            message=f"Model {request.uuid} was trained"
        )
        return __result

    def ModelPredict(self, request, context):
        prediction = mh(ROOT_LOCAL_PATH).predict_model(request.uuid, io.BytesIO(request.x))
        with open(prediction, 'rb') as f:
            file = f.read()
        __result = grpc_pb2.ModelPredictResponse(
            y=file
        )
        return __result

    def ModelDelete(self, request, context):
        mh(ROOT_LOCAL_PATH).delete_model(request.model)
        __result = grpc_pb2.ModelSimpleResponse(
            message=f"Model {request.model} was deleted"
        )
        return __result


