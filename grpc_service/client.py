import time
import grpc
import grpc_pb2
import grpc_pb2_grpc


def run():
    print("List models types")
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = grpc_pb2_grpc.ModelStub(channel)
        response = stub.ModelTypesList(grpc_pb2.Empty())
    print("Result: " + str(response))
    print("---")

    print("Create model")
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = grpc_pb2_grpc.ModelStub(channel)
        response = stub.ModelCreate(grpc_pb2.ModelCreateRequest(tp="LinearRegression", params='{}'))
    print("Result: " + str(response))
    print("---")

    print("List models")
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = grpc_pb2_grpc.ModelStub(channel)
        models = stub.CreatedModelList(grpc_pb2.Empty())
    print("Result: " + str(response))
    print("---")

    print("Train model")
    with open("./data/trainX.csv", 'rb') as file:
        trainX = file.read()
    with open("./data/trainY.csv", 'rb') as file:
        trainY = file.read()
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = grpc_pb2_grpc.ModelStub(channel)
        response = stub.ModelTrain(grpc_pb2.ModelTrainRequest(
            uuid=models.model[-1].split(".")[0],
            x=trainX,
            y=trainY
        ))
    print("Result: " + str(response))
    print("---")

    print("Predict model")
    with open("./data/testX.csv", 'rb') as file:
        testX = file.read()
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = grpc_pb2_grpc.ModelStub(channel)
        response = stub.ModelPredict(grpc_pb2.ModelPredictRequest(
            uuid=models.model[-1].split(".")[0],
            x=testX
        ))
    print("Result: " + str(response))
    print("---")

    print("Delete model")
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = grpc_pb2_grpc.ModelStub(channel)
        response = stub.ModelDelete(grpc_pb2.ModelSimpleRequest(
            model=models.model[-1].split(".")[0],
        ))
    print("Result: " + str(response))
    print("---")


if __name__ == "__main__":
    run()