# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: grpc.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import any_pb2 as google_dot_protobuf_dot_any__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\ngrpc.proto\x1a\x19google/protobuf/any.proto\"\x07\n\x05\x45mpty\"\"\n\x11ModelListResponse\x12\r\n\x05model\x18\x01 \x03(\t\"0\n\x12ModelCreateRequest\x12\n\n\x02tp\x18\x01 \x02(\t\x12\x0e\n\x06params\x18\x02 \x02(\t\"&\n\x13ModelSimpleResponse\x12\x0f\n\x07message\x18\x01 \x02(\t\"7\n\x11ModelTrainRequest\x12\x0c\n\x04uuid\x18\x01 \x02(\t\x12\t\n\x01x\x18\x02 \x02(\x0c\x12\t\n\x01y\x18\x03 \x02(\x0c\".\n\x13ModelPredictRequest\x12\x0c\n\x04uuid\x18\x01 \x02(\t\x12\t\n\x01x\x18\x02 \x02(\x0c\"!\n\x14ModelPredictResponse\x12\t\n\x01y\x18\x01 \x02(\x0c\"#\n\x12ModelSimpleRequest\x12\r\n\x05model\x18\x01 \x02(\t2\xda\x02\n\x05Model\x12.\n\x0eModelTypesList\x12\x06.Empty\x1a\x12.ModelListResponse\"\x00\x12\x30\n\x10\x43reatedModelList\x12\x06.Empty\x1a\x12.ModelListResponse\"\x00\x12:\n\x0bModelCreate\x12\x13.ModelCreateRequest\x1a\x14.ModelSimpleResponse\"\x00\x12\x38\n\nModelTrain\x12\x12.ModelTrainRequest\x1a\x14.ModelSimpleResponse\"\x00\x12=\n\x0cModelPredict\x12\x14.ModelPredictRequest\x1a\x15.ModelPredictResponse\"\x00\x12:\n\x0bModelDelete\x12\x13.ModelSimpleRequest\x1a\x14.ModelSimpleResponse\"\x00')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'grpc_pb2', _globals)
if _descriptor._USE_C_DESCRIPTORS == False:
  DESCRIPTOR._options = None
  _globals['_EMPTY']._serialized_start=41
  _globals['_EMPTY']._serialized_end=48
  _globals['_MODELLISTRESPONSE']._serialized_start=50
  _globals['_MODELLISTRESPONSE']._serialized_end=84
  _globals['_MODELCREATEREQUEST']._serialized_start=86
  _globals['_MODELCREATEREQUEST']._serialized_end=134
  _globals['_MODELSIMPLERESPONSE']._serialized_start=136
  _globals['_MODELSIMPLERESPONSE']._serialized_end=174
  _globals['_MODELTRAINREQUEST']._serialized_start=176
  _globals['_MODELTRAINREQUEST']._serialized_end=231
  _globals['_MODELPREDICTREQUEST']._serialized_start=233
  _globals['_MODELPREDICTREQUEST']._serialized_end=279
  _globals['_MODELPREDICTRESPONSE']._serialized_start=281
  _globals['_MODELPREDICTRESPONSE']._serialized_end=314
  _globals['_MODELSIMPLEREQUEST']._serialized_start=316
  _globals['_MODELSIMPLEREQUEST']._serialized_end=351
  _globals['_MODEL']._serialized_start=354
  _globals['_MODEL']._serialized_end=700
# @@protoc_insertion_point(module_scope)
