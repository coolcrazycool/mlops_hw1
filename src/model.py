import pickle as pkl


class Model:
    """
    Класс для использования конкретной имплементации модели.
    --------
    model : str
        Конкретная модель.
    ------
    train(x=pd.DataFrame, y=pd.DataFrame):
        Обучает инициализированную модель с использованием признаков х и таргета y.
    predict(data=pd.DataFrame):
        Предсказывает таргет на основе поданных признаков data.
    """

    def __init__(self, available_models=None, model_type=None, model_args=None, pkl_path=None):
        """
        На основе переданных параметров инициализирует модель. Если передан путь до модели, то модель считывается из хранилища, если нет, то создается
        ---------
        available_models : dict (default None)
                словарь возможных моделей для использования
        model_type : str (default None)
                запрошенный пользователем тип моедли
        model_args : dict (default None)
                словарь гиперпараметров для инициализиуемой модели
        model_args : dict (default None)
                путь до уже созданной модели на основе ее id
        """
        if pkl_path is None:
            self.model = available_models[model_type](**model_args)
        else:
            self.model = pkl.load(pkl_path)

    def train(self, x, y):
        """
        На основе признаков и таргета обучает модель
        Параметры
        ---------
        x : pd.DataFrame
                признаки для обучения модели
        y : pd.DataFrame
                таргет для обучения модели
        """
        return self.model.fit(x, y)

    def predict(self, data):
        """
        На основе признаков предсказывает таргет
        Параметры
        ---------
        data : pd.DataFrame
                признаки для использования в модели
        """
        return self.model.predict(data)
