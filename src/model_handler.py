from src.model import Model
from uuid import uuid4
import pickle as pkl
from pathlib import Path
import os
import pandas as pd
from fastapi import HTTPException
import subprocess


def dvc_commit_and_push():
    process = subprocess.Popen(["dvc", "commit", "-R", "-f"],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    print("dvc commit... \n", stdout.decode(), stderr.decode())

    process = subprocess.Popen(["dvc", "push"],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    print("dvc push... \n", stdout.decode(), stderr.decode())

class ModelHandler:
    """
        Сервис работы с моделями.
        --------
        path : str
            Корневой путь до рабочей директории сервиса.
        ------
        get_trained_models():
            Получает список всех созданных моделей в рамках сервиса.
        create_model(available_models=dict, model_type=str, model_args=dict):
            Создает модель на основе пользовательского запроса.
        train_model(model_id=str, x=BinaryIO, y=BinaryIO):
            Обучает созданную модель на основе пользовательских данных.
        predict_model(model_id=str, x=BinaryIO):
            Делает предикт на основе пользовательских данных по конкретной модели.
        load_model(model_id=str):
            Загружает из памяти сервиса необходимую модель.
        save_new_model(model):
            Создает в пямяти сервиса новую модель.
        write_model(model, uuid=str):
            Сохраняет модель в память сервиса.
        save_predict(df=pd.DataFrame):
            Сохраняет предикт в память сервиса.
        delete_model(model_id=str):
            Удаляет модель из памяти сервиса.
        """

    def __init__(self, root_path):
        """
        Инициализирует класс работы с моделями на основе корневого пути для сервиса
        :param root_path: Корневой путь для работы с сервисом
        """
        self.path = root_path
        Path(self.path, 'models').mkdir(parents=True, exist_ok=True)
        Path(self.path, 'predictions').mkdir(parents=True, exist_ok=True)

    def get_trained_models(self):
        """
        Получает список всех созданных моделей в рамках сервиса.
        :return: Список уже созданных моделей
        """
        return os.listdir(Path(self.path, 'models'))

    def create_model(self, available_models, model_type, model_args):
        """
        Создает модель на основе пользовательского запроса.
        :param available_models: Словарь возможных моделей для использования
        :param model_type: запрошенный пользователем тип моедли
        :param model_args: словарь гиперпараметров для инициализиуемой модели
        :return: uuid созданной модели
        """
        if model_type in list(available_models.keys()):
            model = Model(available_models, model_type, model_args)
            return self.save_new_model(model)
        else:
            raise HTTPException(status_code=404, detail="Model type not found")

    def train_model(self, model_id, x, y):
        """
        Обучает созданную модель на основе пользовательских данных.
        :param model_id: uuid обучаемой модели
        :param x: признаки для обучения
        :param y: таргет для обучения
        """
        model = self.load_model(model_id)
        try:
            x = pd.read_csv(x)
            y = pd.read_csv(y)
            self.save_train(x, model_id, 'features')
            self.save_train(y, model_id, 'target')
            dvc_commit_and_push()
        except Exception as e:
            raise HTTPException(status_code=400,
                                detail=f"An exception of type {type(e).__name__} occurred. Message: {e}")
        try:
            model.train(x, y)
        except Exception as e:
            raise HTTPException(status_code=500,
                                detail=f"An exception of type {type(e).__name__} occurred. Message: {e}")
        self.write_model(model, model_id)

    def predict_model(self, model_id, x):
        """
        Делает предикт на основе пользовательских данных по конкретной модели.
        :param model_id: uuid модели для предсказания
        :param x: признаки для предсказания
        :return: путь до файла с предсказанием в памяти сервиса
        """
        model = self.load_model(model_id)
        try:
            x = pd.read_csv(x)
        except Exception as e:
            raise HTTPException(status_code=400,
                                detail=f"An exception of type {type(e).__name__} occurred. Message: {e}")
        try:
            predict = model.predict(x)
        except Exception as e:
            raise HTTPException(status_code=500,
                                detail=f"An exception of type {type(e).__name__} occurred. Message: {e}")
        x['predict'] = predict
        return x

    def load_model(self, model_id):
        """
        Загружает из памяти сервиса необходимую модель.
        :param model_id: uuid модели для загрузки
        :return: прочитанная модель
        """
        if Path(self.path, 'models', f"{model_id}.pickle").exists():
            with open(Path(self.path, 'models', f"{model_id}.pickle"), 'rb') as file:
                model = pkl.load(file)
            return model
        else:
            raise HTTPException(status_code=404, detail="Model not found")

    def save_new_model(self, model):
        """
        Создает в пямяти сервиса новую модель.
        :param model: модель для создания
        :return: uuid модели в памяти сервиса
        """
        uuid = uuid4()
        self.write_model(model, uuid)
        return uuid


    def write_model(self, model, uuid):
        """
        Сохраняет модель в память сервиса.
        :param model: модель для создания
        :param uuid: uuid модели в памяти сервиса
        """
        with open(Path(self.path, 'models', f"{uuid}.pickle"), 'wb') as file:
            pkl.dump(model, file)
        dvc_commit_and_push()


    def delete_model(self, model_id):
        """
        Удаляет модель из памяти сервиса.
        :param model_id: uuid модели для удаления
        """
        if Path(self.path, 'models', f"{model_id}.pickle").exists():
            os.remove(Path(self.path, 'models', f"{model_id}.pickle"))
        else:
            raise HTTPException(status_code=404, detail="Model not found")

    def save_train(self, df, uuid, tp):
        """
        Сохраняет трейн.
        :param df: DataFrame с предсказанием
        :return: путь до файла в памяти сервиса с предсказанием
        """
        df.to_csv(Path(self.path, 'train', f"{uuid}_{tp}.csv"))
        return Path(self.path, 'train', f"{uuid}_{tp}.csv")