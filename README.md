#### REST API

Start with `uvicorn main:app --reload`

Swagger in `http://127.0.0.1:8000/docs`

#### gRPC

Start with running `./grpc_service/app.py`

Test with `./grpc_service/client.py`

